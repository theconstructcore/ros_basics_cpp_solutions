cmake_minimum_required(VERSION 2.8.3)
project(my_custom_srv_msg_cpp_pkg)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  message_generation
)


add_service_files(
   FILES
   MyCustomServiceMessage.srv
 )

generate_messages(
   DEPENDENCIES
   std_msgs
 )


catkin_package(
  CATKIN_DEPENDS roscpp
)

include_directories(include ${catkin_INCLUDE_DIRS})


add_executable(custom_service_server src/custom_service_server.cpp)
target_link_libraries(custom_service_server ${catkin_LIBRARIES})
add_dependencies(custom_service_server ${my_custom_srv_msg_cpp_pkg_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})