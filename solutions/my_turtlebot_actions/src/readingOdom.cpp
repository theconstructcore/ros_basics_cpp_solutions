#include <ros/ros.h>
#include <std_msgs/Empty.h>
#include <nav_msgs/Odometry.h> 
#include <math.h>

void giveOdom(const nav_msgs::Odometry::ConstPtr& msg){
    //ROS_INFO("%s", msg->header.frame_id.c_str());
    //ROS_INFO("%f", msg->twist.twist.linear.x);
  ROS_INFO("This is the x %f", msg->pose.pose.position.x);
  ROS_INFO("This is the y %f", msg->pose.pose.position.y);
}


int main(int argc, char** argv) {
  ros::init(argc,argv, "sub_odom"); //name of node
  ros::NodeHandle nh;
 
 ros::Subscriber sub_read = nh.subscribe<nav_msgs::Odometry>("odom",1000,giveOdom); //calls the callback function
 
 ros::spin();
 return 0;   
}