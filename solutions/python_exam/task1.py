from robot_control_class import RobotControl

def get_highest_lowest():    
    rc = RobotControl()
    laser_values = rc.get_laser_full() 
    max = 0
    max_ind = 0
    min = 100000
    min_ind =0
    #print (laser_values[151:203])
    #print (len(laser_values))
    for i in range(0,len(laser_values)):
        if i >= 151 and i < 203:
            continue
        else:
            if laser_values[i]>max:
                max = laser_values[i]
                max_ind = i
    for i in range(0,len(laser_values)):
        if i>=151 and i<203:
            continue
        else:
            if laser_values[i]<min:
                min = laser_values[i]
                min_ind = i
    #print (max,max_ind)
    #print (min,min_ind)

    return max_ind,min_ind
#get_highest_lowest()
